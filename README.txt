INTRO
=====
This module adds a checkout pane to Ubercart's checkout page for saving user
profile data. It makes use of the core Profile module (not Content Profile).  
The profile pane can be configured to pull from any category of the user's profile.


INSTALLATION
============

0) Enable and configure the core Drupal Profile module, and Ubercart.

1) Place this module directory in your "modules" folder (this will usually be
   "sites/all/modules/"). Don't install your module in Drupal core's "modules"
   folder, since that will cause problems and is bad practice in general. If
   "sites/all/modules" doesn't exist yet, just create it.

2) Enable the UC Profile module in Drupal at:
   administration -> site configuration -> modules (admin/build/modules)

3) Enable the pane at 'admin/store/settings/checkout/edit/panes', and choose which

4) Set the pane's title under the Profile Panes settings.
  This will also change the title of the pane on the profile page
  and anywhere else the profile is displayed.
  
5) Choose which profile sections to display in the checkout pane.